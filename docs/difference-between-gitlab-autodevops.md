# Cactus AutoDevOps vs GitLab AutoDevOps

[GitLab AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/) allows for projects to inherit predefined CI configurations.

This allows project maintainers to no longer need to write/maintain a CI configuration, provided they use a common code layout.

GitLab AutoDevOps by default only works with very simple projects, which most of the cactus projects are not.
In addition GitLab AutoDevOps does not know about CERN services like EOS or P5 Dropbox, and in fact works against CERN infrastructure in some ways
(like requiring docker-in-docker support and wanting to install PostgreSQL).

This Cactus-adapted set of templates work the same as GitLab AutoDevOps, but understands cactus code structures and CERN services.

Reasons why bare GitLab AutoDevops was not used include:
- only the simplest project structures are supported
- at the time of writing (aug 2020), custom project layout support requires
  knowledge about CNCF BuildPacks as well as the already deprecated
  heroku system, since CNCF BuildPacks do not (yet) support auto-testing
  - this is deemed too much learning + maintenance work for the cactus group
  - it provides no clear way on how to implement other things we want to do,
    like deploying to an RPM repository.
- K8S integration requires access to the cluster-admin account
  - this account info is available in all AutoDevOps enabled CI jobs
- at the time of writing (aug 2020), K8S integration fails on cluster version >1.16 (2 versions behind)
- K8S deployments sporadically got stuck or out of sync, with no good way to un-stuck them, even with manual kubectl intervention (tends to make it worse).
- AutoDeploy activates on push to the default (master) branch, while our community is very clear that they want explicit deploys to production (push a button, and/or apply a tag).
- The GitLab AutoDevOps images require docker-in-docker to be available. CERN GitLab
  runners do not have this.
- GitLab AutoDevOps *always* installs a PostgreSQL instance alongside your deployment. It assumes you'll always need it, and GitLab upgrades bring breaking changes to its deployment.

GitLab AutoDevOps does provide extra project interface extensions, such as
[AutoMonitoring](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-monitoring)
Cactus AutoDevOps tries to enable these as well wherever possible.

Other areas where Cactus AutoDevOps follows GitLab AutoDevOps:
- use of the `KUBE_INGRESS_BASE_DOMAIN` CI variable on cms-cactus group level
- built-in letsencrypt service for k8s ingress deployments
- [AutoMonitoring](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-monitoring)

Cactus AutoDevOps deviates from GitLab AutoDevOps in the following ways:
- explicitly enabled  
  Cactus AutoDevOps tries not to have too much [magic](http://www.catb.org/~esr/jargon/html/M/magic.html). It will not enable any of its functionality on your project unless explicitly told to.
- explicitly configured  
  For example, the letsencrypt service will not be invoked on your deployment without you asking for it with the `cert-manager.io/cluster-issuer: letsencrypt` annotation.