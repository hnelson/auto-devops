package internal

import (
	"flag"
	"sort"
	"strings"

	"github.com/pkg/errors"
)

type userData map[string]string

// String sorts keys and joins key1=value,key2=value pairs
func (u *userData) String() string {
	a := make([]string, 0)

	sortedKeys := make([]string, 0)
	for k := range *u {
		sortedKeys = append(sortedKeys, k)
	}
	sort.Strings(sortedKeys)
	for i := range sortedKeys {
		a = append(a, sortedKeys[i]+"="+(*u)[sortedKeys[i]])
	}
	return strings.Join(a, ",")
}

// Set adds an extra key=value pair to userData
func (u *userData) Set(value string) error {
	split := strings.SplitN(value, "=", 2)
	if len(split) != 2 {
		return errors.New("data does not contain '=' character")
	}
	(*u)[split[0]] = split[1]
	return nil
}

var myFlags = make(userData)
var cachedArgs *Args

// Args contain CLI arguments
type Args struct {
	Verbose  bool
	Filename string
	UserData map[string]string
}

// ParseArgs parses arguments into an Args for you
func ParseArgs() (*Args, error) {
	if cachedArgs != nil {
		return cachedArgs, nil
	}
	verbose := flag.Bool("v", false, "verbose output")
	flag.Var(&myFlags, "data", "adds arbitrary template data to .UserData")
	flag.Parse()
	if len(flag.Args()) != 1 {
		return nil, errors.New("no file given as argument")
	}
	filename := flag.Args()[0]
	cachedArgs = &Args{*verbose, filename, myFlags}
	return cachedArgs, nil
}
