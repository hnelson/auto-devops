package internal

import (
	"text/template"

	"github.com/Masterminds/sprig"
)

// GetTemplate gives a template instance with all functions wired up
func GetTemplate(name string) *template.Template {
	return template.New(name).Funcs(sprig.TxtFuncMap()).Funcs(FuncMap())
}
