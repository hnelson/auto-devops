package internal

import (
	"bytes"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetTemplate(t *testing.T) {
	m := GetTemplate("name")
	require.NotNil(t, m)
}

type testExamples struct {
	template string
	output   string
	envVars  map[string]string
}

func TestExamples(t *testing.T) {
	examples := []testExamples{
		{`name: "{{ slug 63 (env "CI_PROJECT_NAME") "-" (env "CI_COMMIT_REF_NAME") }}"`, `name: "ci-project-name-ci-commit-ref-name"`, map[string]string{}},
		{`name: "{{ slug 20 (env "CI_PROJECT_NAME") "-" (env "CI_COMMIT_REF_NAME") }}"`, `name: "project-name-waaaaaa"`, map[string]string{"CI_PROJECT_NAME": "project_name-WAAAAAAAAAY-too-long"}},
		{`{{ call .Generator.GetGitlabFlowImageName }}`, `CI_REGISTRY_IMAGE:tag-CI_COMMIT_TAG`, map[string]string{}},
		{`{{ .UserData.a }}`, `b`, map[string]string{}},
	}

	defer func() {
		os.Unsetenv("CI_PROJECT_NAME")
		os.Unsetenv("CI_REGISTRY_IMAGE")
		os.Unsetenv("CI_COMMIT_TAG")
		os.Unsetenv("CI_COMMIT_REF_NAME")
		os.Unsetenv("CI_COMMIT_SHORT_SHA")
	}()

	_template := GetTemplate("testexamples")
	require.NotNil(t, _template)

	data := GetTemplateValues(map[string]string{"a": "b"})

	for _, task := range examples {
		os.Setenv("CI_PROJECT_NAME", "CI_PROJECT_NAME")
		os.Setenv("CI_REGISTRY_IMAGE", "CI_REGISTRY_IMAGE")
		os.Setenv("CI_COMMIT_TAG", "CI_COMMIT_TAG")
		os.Setenv("CI_COMMIT_REF_NAME", "CI_COMMIT_REF_NAME")
		os.Setenv("CI_COMMIT_SHORT_SHA", "CI_COMMIT_SHORT_SHA")

		for k, v := range task.envVars {
			os.Setenv(k, v)
		}

		template, err := _template.Parse(task.template)
		require.NoError(t, err)

		var buffer bytes.Buffer
		err = template.Execute(&buffer, data)
		fmt.Println(buffer.String())
		require.NoError(t, err)
		require.Equal(t, task.output, buffer.String())
	}

}
