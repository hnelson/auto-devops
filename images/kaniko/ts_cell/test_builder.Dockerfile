ARG SWATCH_IMAGE=gitlab-registry.cern.ch/cms-cactus/core/swatch/xdaq15-swatch14:master-latest
FROM $SWATCH_IMAGE

ARG XDAQ_CONFIG
ARG XDAQ_PROFILE
ARG XDAQ_PORT=3333


##############################
#### Install RPM Packages ####
##############################
# copy and install rpm packages

COPY ci_rpms /root/rpm/
RUN yum install -y /root/rpm/*.rpm \
    && yum install -y cmsos-xaas-trigger-extension \
    && yum clean all 

# Do I need them?
###############################
#### Environment Variables ####
###############################
ENV PID_DIR=/var/run
ENV LOCK_DIR=/var/lock
ENV CORE_DIR=/tmp
ENV XDAQ_ZONE=trigger
ENV XDAQ_EXTERN_LIBRARY_PATH=/opt/xdaq/lib
ENV XDAQ_ROOT=/opt/xdaq
ENV XDAQ_SETUP_ROOT=/opt/xdaq/share
ENV XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs
ENV LD_LIBRARY_PATH=/opt/xdaq/lib:/opt/cactus/lib
ENV XDAQ_USER=root
ENV XDAQ_LOG=/var/log/trigger-cell.log

# The HOSTNAME variable will be set in the startup.sh script to be flexible with different hosts, so it not required at build time
# ENV XDAQ_HOSTNAME=crashtestdummy.cern.ch

# The port depends on the application you want to run. Normally it should be 3333, but TCDS runs un 4500
ENV XDAQ_PORT=${XDAQ_PORT}
EXPOSE ${XDAQ_PORT}

# Set environment variables for the profile and configure files which contain the information to set up a specific trigger cell.
# The files come frome the cmsos-xaas-trigger-extension package installed above.
ENV XDAQ_CONFIG=${XDAQ_CONFIG}
ENV XDAQ_PROFILE=${XDAQ_PROFILE}
# ENV XDAQ_PROFILE=/opt/xdaq/share/trigger/profile/calol1-cell.profile
# ENV XDAQ_CONFIG=/opt/xdaq/share/trigger/profile/calol1-cell.configure
# ENV XDAQ_PROFILE=/opt/xdaq/share/trigger/profile/bmtf-cell.profile
# ENV XDAQ_CONFIG=/opt/xdaq/share/trigger/profile/bmtf-cell.configure
# ENV XDAQ_PROFILE=/opt/xdaq/share/trigger/profile/tcds-gtup-ici.profile
# ENV XDAQ_CONFIG=/opt/xdaq/share/trigger/profile/ugt-cell.configure

#############################
#### Change XDAQ Configs ####
#############################
# The files don't run out of the box because we can't access some urls as this test setup is outside of the cms system.
# So we need to do the following replacements or remove some parts, so the cell still runs:

# In the configure file remove all the GTUP TCDS applications (important to run the tcds cell). Needs to run bevor the next sed command which replaces the urls.
RUN sed -i '/<xc:Context url=.http:\/\/tcds-control-trigger-.*/,/<.xc:Context>/d' ${XDAQ_CONFIG}

# "In the configure file, remove the LAS Application.
RUN sed -i '/<xc:Context.*kvm-s3562-1-ip151-102.cms:9945.*/,/<.xc:Context>/d' ${XDAQ_CONFIG}

# In the profile file, remove the trace probe application.
RUN sed -i "/<xp:Application.*class=.tracer/,/<\/xp:Application>/d" ${XDAQ_PROFILE}

####################
#### Start XDAQ ####
####################
# run xdaq with bmtf profile and configs
# The script gets added in the cactus-buildenv base image (https://gitlab.cern.ch/cms-cactus/core/cactus-buildenv/-/blob/master/base.Dockerfile#L22-24)
ENTRYPOINT ["/opt/cactus/test/start_ts_cell.sh"]
