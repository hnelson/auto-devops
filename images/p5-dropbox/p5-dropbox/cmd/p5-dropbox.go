package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"p5-dropbox/internal"
)

var viaLXPlus = flag.Bool("lxplus", false, "set true to pass via lxplus first")

func main() {
	fmt.Println("my args", os.Args)
	flag.Parse()
	filenamesToCopy := flag.Args()
	if len(filenamesToCopy) == 0 {
		flag.Usage()
		log.Fatal("no RPM files specified")
	}
	filesToCopy := make([]*internal.SCPJob, len(filenamesToCopy))
	var err error
	for i, name := range filenamesToCopy {
		filesToCopy[i], err = internal.SCPJobFrom(name)
		if err != nil {
			log.Fatalf("cannot stat input file %s: %s", name, err)
		}
	}

	dropboxJob, err := internal.GetDropboxArgs()
	if err != nil {
		log.Fatalf("cannot construct dropbox command: %s", err)
	}

	sshClient, pass, err := internal.GotoCMSDropbox(*viaLXPlus)
	if err != nil {
		log.Fatalf("cannot connect to cmsdropbox: %s", err)
	}

	err = internal.Mkdir(sshClient, dropboxJob.RPMFolder)
	if err != nil {
		log.Fatalf("cannot mkdir: %s", err)
	}
	log.Println("folder created ", dropboxJob.RPMFolder)
	for _, job := range filesToCopy {
		log.Println("copying ", job.Info.Name())
		err = internal.Copy(sshClient, dropboxJob.RPMFolder, job)
		if err != nil {
			log.Fatalf("could not execute copy command: %s", err)
		}
		log.Println("copy finished ", job.Info.Name())
	}
	err = internal.Dropbox2(sshClient, pass, dropboxJob)
	internal.Rmdir(sshClient, dropboxJob.RPMFolder)
	if err != nil {
		log.Fatalf("could not execute dropbox2 command: %s", err)
	}
}
