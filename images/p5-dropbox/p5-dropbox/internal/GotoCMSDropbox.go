package internal

import (
	"fmt"
	"log"

	"golang.org/x/crypto/ssh"
)

// GotoCMSDropbox connects via SSH to the dropbox host in the CMS network
func GotoCMSDropbox(viaLXPlus bool) (*ssh.Client, string, error) {
	var cmsUsrClient *ssh.Client
	var err error
	if viaLXPlus {
		lxplusconf, _, _ := GetSSHConfig("lxplus.cern.ch")
		lxplus, err := ssh.Dial("tcp", "lxplus.cern.ch:22", lxplusconf)
		if err != nil {
			return nil, "", fmt.Errorf("could not dial lxplus: %s", err)
		}
		log.Println("connected to lxplus")
		cmsUsrClient, _, _, err = dialThrough(lxplus, "cmsusr")
	} else {
		cmsUsrConf, _, _ := GetSSHConfig("cmsusr")
		cmsUsrClient, err = ssh.Dial("tcp", "cmsusr:22", cmsUsrConf)
	}
	if err != nil {
		return nil, "", fmt.Errorf("could not open SSH client on cmsusr: %s", err)
	}
	log.Println("connected to cmsusr")
	cmsDropboxClient, _, pass, err := dialThrough(cmsUsrClient, "cmsdropbox")
	if err != nil {
		return nil, "", fmt.Errorf("could not open SSH client on cmsdropbox: %s", err)
	}
	log.Println("connected to cmsdropbox")
	return cmsDropboxClient, pass, nil
}
