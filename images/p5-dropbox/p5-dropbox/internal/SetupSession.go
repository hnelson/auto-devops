package internal

import (
	"bytes"
	"io"

	"golang.org/x/crypto/ssh"
)

// SetupSession creates an SSH session from a given client
func SetupSession(client *ssh.Client) (*ssh.Session, *io.WriteCloser, *bytes.Buffer, *bytes.Buffer, error) {
	session, err := client.NewSession()
	if err != nil {
		return nil, nil, nil, nil, err
	}

	var stdoutBuf bytes.Buffer
	session.Stdout = &stdoutBuf
	var stderrBuf bytes.Buffer
	session.Stderr = &stdoutBuf

	stdin, err := session.StdinPipe()
	if err != nil {
		return nil, nil, nil, nil, err
	}
	return session, &stdin, &stdoutBuf, &stderrBuf, nil
}
