package internal

import (
	"bytes"
	"log"
)

// DumpStd takes stdout and stderr buffers and prints them
func DumpStd(stdout *bytes.Buffer, stderr *bytes.Buffer) {
	log.Printf("stdout>\n%s", stdout.String())
	log.Printf("stderr>\n%s", stderr.String())
}
