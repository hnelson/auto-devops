package internal

import (
	"fmt"
	"io"
	"log"
	"os"
	"path"

	"golang.org/x/crypto/ssh"
)

// Copy copies a local file to an ssh remote in basePath using the same filename
func Copy(client *ssh.Client, basePath string, job *SCPJob) error {
	session, stdin, stdout, stderr, err := SetupSession(client)
	if err != nil {
		return err
	}
	defer session.Close()

	go func() {
		defer (*stdin).Close()
		fmt.Fprintf(*stdin, "C%#o %d %s\n", os.ModePerm, job.Info.Size(), job.Info.Name())
		io.Copy(*stdin, job.File)
		fmt.Fprint(*stdin, "\x00")
	}()

	p := path.Join(basePath, job.Info.Name())
	err = session.Run("scp -t " + p)
	if err != nil {
		DumpStd(stdout, stderr)
		log.Printf("error while copying %s -> %s: %s\n", job.LocalPath, p, err)
		return err
	}
	return err
}
