package internal

import (
	"bytes"
	"fmt"
	"log"
	"strings"

	"golang.org/x/crypto/ssh"
)

// Mkdir performs mkdir -p on an ssh remote
func Mkdir(client *ssh.Client, path string) error {
	session, err := client.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()
	var stdoutBuf bytes.Buffer
	session.Stdout = &stdoutBuf
	var stderrBuf bytes.Buffer
	session.Stderr = &stdoutBuf
	err = session.Run("mkdir -p " + path)
	if err != nil {
		DumpStd(&stdoutBuf, &stderrBuf)
		log.Println("mkdir failed", path, stdoutBuf.String(), stderrBuf.String())
	}
	return err
}

// Rmdir performs rm -rf on an ssh remote, only accepts paths in /tmp
func Rmdir(client *ssh.Client, path string) error {
	session, err := client.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()
	var stdoutBuf bytes.Buffer
	session.Stdout = &stdoutBuf
	var stderrBuf bytes.Buffer
	session.Stderr = &stdoutBuf
	if !strings.HasPrefix(path, "/tmp") {
		log.Println("not deleting ", path)
		return fmt.Errorf("path to delete is not in /tmp folder: " + path)
	}
	err = session.Run("rm -rf " + path)
	if err != nil {
		DumpStd(&stdoutBuf, &stderrBuf)
		log.Println("mkdir failed", path, stdoutBuf.String(), stderrBuf.String())
	}
	return err
}
