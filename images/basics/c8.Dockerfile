FROM cern/c8-base:20200701-2.x86_64
LABEL maintainer="Cactus <cactus@cern.ch>"

RUN dnf install -y make git rpm-build rubygems jq \
                   gcc gcc-c++ ruby-devel && \
    gem install -f fpm && \
    dnf clean all

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["/bin/bash"]