# Available templates

This is a list of all available templates accompanied with a short description.
More details are available in the documentation dedicated to that template (linked below).

Most use cases only need configuration of these templates via `variables: ` key-value pairs.  
You can however override these templates in any way you see fit, but be mindful of GitLab CI's [merging logic](https://docs.gitlab.com/ee/ci/yaml/#merge-details).  
Most notably, it does not deep-merge arrays (like `script:`), and empty maps will clear any inherited map (like empty `variables:`).

# Docker
[Docker builder](templates/docker-builder.md) builds and pushes docker images to your repo's container registry. Supports auto-tagging.

# Documentation
[Markdown HTML compiler](templates/markdown-to-html.md) compiles all markdown files into html files. Applies a default theme.

[Sphinx compiler](templates/sphinx.md) compiles sphinx documentation. Defaults to html compilation but can be any other supported format.

# EOS
[EOS/CERNBOX sync](templates/upload-eos.md) can send/receive files to/from EOS.
Useful to deploy to an EOS/CERNBox backed website.

It can also upload RPMs to an EOS backed YUM repo and update the database files there.

# Deployment

[P5 Dropbox push](templates/p5-dropbox-push.md) can send RPMs to the P5 dropbox system.

[Kubernetes deployer](templates/kubectl.md) contains kubectl, a powerful templating engine, and ready to use example template manifests.
  Useful for deployments to kubernetes clusters such as the [Cactus Review](https://gitlab.cern.ch/cms-cactus/ops/cactus-review) cluster.  
  Read the documentation for the cluster you deploy to to learn about any available services (for example Cactus Review has Letsencrypt integration).

[CERN OpenShift deployer](templates/openshift.md) contains content from the Kubernetes deployer, but the kubectl binary and the extra oc binary are sourced from CERN OpenShift.