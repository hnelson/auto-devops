# kubectl template

## Static site deployment
The most basic use for a kubernetes cluster is to host a static site.

Sort of like using the LHC to heat up soup.

```yaml
docs:
  stage: stage name
  extends: .auto_devops_k8s_static_site
  variables:
    # path to folder containing the static site content
    # much like the htdocs folder in the old days
    CONTENT_DIR: build/docs
    # name of the deployment for this static site
    CONTENT_NAME: docs
  environment:
    on_stop: 💣 docs

💣 docs:
  stage: stage name
  extends: .auto_devops_k8s_static_site_stop
  dependencies: ["docs"]
  variables:
    CONTENT_NAME: docs
```

## Basic deployment

The type of deployment you probably want to do consists of a `deployment` + `service` + `ingress` + `secret`

```yaml
app:
  stage: deploy
  extends: .auto_devops_k8s_basic_deployment
  environment:
    on_stop: 💣 app

💣 app:
  stage: deploy
  extends: .auto_devops_k8s_basic_deployment_stop
  dependencies: ["app"]
```

Optionally, you can provide a `DOCKER_IMAGE_NAME` variable that corresponds to the `NAME` variable in a [docker-builder](./docker-builder.md) job
earlier in your pipeline.

### Features
- This template will take any `K8S_SECRET_NAME=VALUE` variables in your GitLab CI pipeline and put them in a secret as `NAME=VALUE`
- This template automatically generates a HTTPS certificate for your deployment via letsencrypt

Extra details if you are unfamiliar with kubernetes:
- a `pod` is what your code runs in. Can consist of multiple docker containers but in this template a pod = your docker container.
- a `deployment` runs a configured amount of replicas of pods (in case of this template: 1), and handles restarts and upgrades.
- a `service` publishes your deployment (or a combination of multiple) under a logical name inside kubernetes.
- an `ingress` publishes a service on an externally reachable URI
- a `secret` is a secure set of key=value pairs that can be attached to a pod.

## Custom deployment

The template gives you bare control over kubectl and the templating engine.

```yaml
app:
  stage: deploy
  extends: .auto_devops_kubectl_runner
  script:
  - mkdir -p build/manifests
  - rendersecretstemplate | rendertemplate -v - > build/manifests/secret.yml
  - rendertemplate -v /opt/k8s/templates/basicdeployment.yaml | tee build/manifests/fromtemplate.yml
  - rendertemplate -v k8s/myowntemplate.yaml | tee build/manifests/fromrepo.yml
  - kubectl --kubeconfig="$K8S_CONFIG" apply -Rf build/manifests
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_COMMIT_REF_SLUG.$CI_PROJECT_NAME.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: 💣 app
  artifacts:
    paths:
    - build/manifests

💣 app:
  stage: deploy
  extends: .auto_devops_kubectl_runner
  dependencies: ["app"] # download *only* manifests from app job
  when: manual
  variables:
    # Setting the GIT_STRATEGY to none is necessary in the stop_review job so 
    # that the GitLab Runner won’t try to check out the code after the branch is deleted.
    GIT_STRATEGY: none
  script:
  - kubectl --kubeconfig="$K8S_CONFIG" delete -Rf build/manifests
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
```


### Available templates

- [/opt/k8s/templates/basicdeployment.yaml](../images/kubectl/opt/k8s/templates/basicdeployment.yaml)
Covers a basic deployment. Contains a `Deployment`, `Service`, and `Ingress`.
The ingress is configured to use the Letsencrypt service from [Cactus Review](https://gitlab.cern.ch/cms-cactus/ops/cactus-review).

- [/opt/k8s/templates/staticwebsite.yaml](../images/kubectl/opt/k8s/templates/staticwebsite.yaml)
Variant of `basicdeployment` for static website deployments.

## Secrets generator

`rendersecretstemplate` is useful in a CI environment. It scans all environment variables for any `K8S_SECRET_*` variables and puts them in a K8S Secrets file without the `K8S_SECRET_` prefix.

```yaml
script:
- rendersecretstemplate | rendertemplate -v - > secret.yml
```

## Template parser

You have the option to use [Go templates](https://golang.org/pkg/text/template/) in your manifests.

For example, this template yaml file
```yaml
apiVersion: v1
kind: Service
metadata:
  name: "{{ env "CI_PROJECT_NAME") }}-{{ env "CI_COMMIT_REF_NAME" }}"
```
can be parsed and deployed using
```yaml
script:
- rendertemplate -v template.yaml > parsed.yaml
- kubectl --kubeconfig="$K8S_CONFIG" apply -f parsed.yaml
```
`parsed.yaml` would look like this
```yaml
apiVersion: v1
kind: Service
metadata:
  name: "myproject-master"
```

### Available functions and data

The details of the template parser are documented [here](../images/kubectl/rendertemplate/README.md).