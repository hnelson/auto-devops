# GitLab AutoDevOps for Cactus projects

Cactus AutoDevOps is a collection of GitLab CI templates and images designed to ease
development and maintenance of CI workflows for Cactus projects.

It is built on the same foundations as [GitLab AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/),
but tailored for CERN and CMS infrastructure.
You can read more about differences between GitLab AutoDevOps [here](docs/difference-between-gitlab-autodevops.md).

## Real-world examples

- The [Level-1 Configuration Editor](https://gitlab.cern.ch/cms-cactus/web/L1CE) builds RPMs and docker images, deploys feature branches to a Kubernetes cluster, and deploys the master branch to CERN OpenShift.

- The [CMS Rate Monitoring tool](https://gitlab.cern.ch/cms-tsg-fog/ratemon) automatically builds RPMs and deploys to P5.

- [SWATCH](https://gitlab.cern.ch/cms-cactus/core/swatch) builds and publishes RPMs, and deploys a demo cell to Kubernetes.

- [CaloL1](https://gitlab.cern.ch/cms-cactus/projects/calol1) and [CaloL2](https://gitlab.cern.ch/cms-cactus/projects/calol2) use the XDAQ15 builder preset.

## Demo examples

- [Empty XDAQ15 SWATCH Cell](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/example-swatch-cell)
- [Generic Makefile Example](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/makefile-full)
- [Docker project](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/docker)

## Quick-start
In your `.gitlab-ci.yml` file, you need to import Cactus AutoDevOps.

If you use a known standard layout, you activate it like so:
```yaml
# .gitlab-ci.yml
# import and activate the SWATCH Cell standard pipeline
# no other code than this is needed
include:
  - project: cms-cactus/ops/auto-devops
    ref: 0.2.3
    file: presets/swatch-cell-xdaq15.yml
```

If you want to build your own pipeline on top of Cactus AutoDevOps functionality:
```yaml
# .gitlab-ci.yml
# import all Cactus AutoDevOps templates
include:
  - project: cms-cactus/ops/auto-devops
    ref: 0.2.3
    file: presets/blank-slate.yml

stages:
- 🏗️ build

# example how to implement a template (simplest case, check docs for more elaborate examples)
builder image:
  extends: .auto_devops_docker_builder_autotag
  stage: 🏗️ build
```
Example projects using these templates can be found in the [auto-devops-examples](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples) group.  

## Available presets
- [XDAQ15 SWATCH Cell](presets/swatch-cell-xdaq15.md)

## Available templates
A more elaborate version of this list can be found [here](templates/README.md).

- [Docker builder](templates/docker-builder.md)
- [Markdown HTML compiler](templates/markdown-to-html.md)
- [Sphinx compiler](templates/sphinx.md)
- [EOS/CERNBOX sync](templates/upload-eos.md)
- [P5 Dropbox push](templates/p5-dropbox-push.md)
- [Kubernetes deployer](templates/kubectl.md)
- [CERN OpenShift deployer](templates/openshift.md)